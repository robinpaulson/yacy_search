package de.audioattack.yacy31c3search.ui.NavDrawer;

/**
 * @author Marc Nause <marc.nause@gmx.de>
 */
public class DrawerItem {
    public final int text;
    public final int icon;

    public DrawerItem(final int textId, final int itemResId) {

        this.text = textId;
        this.icon = itemResId;
    }
}
